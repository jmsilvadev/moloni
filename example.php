<?php
include 'vendor/autoload.php';

use Moloni\Customers;
use Moloni\Suppliers;
use Moloni\Languages;
use Moloni\PaymentMethods;
use Moloni\Countries;
use Moloni\MaturityDates;
use Moloni\Salesmen;
use Moloni\DeliveryMethods;
use Moloni\Config;


//Example to change Credentials
$arrCredentials = [
    "DEVELOPER_ID"  => "fake_id",
    "CLIENT_SECRET" => "fake_secret",
    "USERNAME" => "fake_username",
    "PASSWORD" => "fake_password"
];
//Uncoment this line to change the credentials
Config::setAuth($arrCredentials);

//Example to get Clients
$arrBody = ["company_id" => 65482];
$moloni = new Customers();
$arrRes = $moloni->getAll($arrBody);
//To see results uncomment lines below
//echo "CLIENTS:" . PHP_EOL;
//print_r($arrRes);

//Example to get Suppliers
$arrBody = ["company_id" => 65482, "supplier_id" => 2];
$moloni = new Suppliers();
$arrRes = $moloni->getAll($arrBody);
//To see results uncomment lines below
//echo "SUPPLIERS:" . PHP_EOL;
//print_r($arrRes);

//Example to Insert Clients
//According documentation of site Moloni (https://www.moloni.pt/dev/index.php?action=getApiDocDetail&id=204)
//We need some mandatory informations, because of that we need to call other methods before execute the insert
$moloni_lang = new Languages();
$arrLang = $moloni_lang->getAll();
//To see results uncomment lines below
//echo "LANGUAGES:" . PHP_EOL;
//print_r($arrLang);

$moloni_coun = new Countries();
$arrCountries = $moloni_coun->getAll();
//To see results uncomment lines below
//echo "COUNTRIES:" . PHP_EOL;
//print_r($arrCountries);

$moloni_dates = new MaturityDates();
$arrBody = ["company_id" => 65482];
$arrMatDates = $moloni_dates->getAll($arrBody);
//To see results uncomment lines below
//echo "MATURITY DATES:" . PHP_EOL;
//print_r($arrMatDates);

$moloni_pay = new PaymentMethods();
$arrBody = ["company_id" => 65482];
$arrPay = $moloni_pay->getAll($arrBody);
//To see results uncomment lines below
//echo "PAYMENTS:" . PHP_EOL;
//print_r($arrPay);

$moloni_salelsman = new Salesmen();
$arrBody = ["company_id" => 65482];
$arrSalesman = $moloni_salelsman->getAll($arrBody);
//To see results uncomment lines below
//echo "SALESMAN:" . PHP_EOL;
//print_r($arrSalesman);

$moloni_delivery= new DeliveryMethods();
$arrBody = ["company_id" => 65482];
$arrDelivery = $moloni_delivery->getAll($arrBody);
//To see results uncomment lines below
//echo "DELIVERY:" . PHP_EOL;
//print_r($arrDelivery);

$arrBody = [
            "company_id" => 65482,
            "name" => "Test Example",
            "number" => "CLI-9997", //Change to other code
            "vat" => "111111122", //Change to other vat number
            "address" => "Example Address",
            "city" => "Test Example",
            "zip_code" => "1111-111",
            "payment_day" => 5,
            "discount" => 5,
            "credit_limit" => 5,
            "delivery_method_id" => $arrDelivery[0]["delivery_method_id"],
            "salesman_id" => $arrSalesman[0]["salesman_id"],
            "language_id" => $arrLang[0]["language_id"],
            "country_id" => $arrCountries[1]["country_id"],
            "maturity_date_id" => $arrMatDates[0]["maturity_date_id"],
            "payment_method_id" => $arrPay[1]["payment_method_id"]
        ];

$moloni = new Customers();
$arrRes = $moloni->insert($arrBody);
//To see results uncomment lines below
echo "INSERT CLIENT:" . PHP_EOL;
print_r($arrRes);

