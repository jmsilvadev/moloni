# Wrapper Moloni 2019

This wrapper is an abstraction of Moloni API.

***

## Status
[![status](https://img.shields.io/static/v1.svg?label=status&message=Released&color=#97cabf)](https://github.com/jmsilvadev/Moloni2019)
[![version](https://img.shields.io/static/v1.svg?label=version&message=v1.0.0&color=#97cabf)](https://github.com/jmsilvadev/Moloni2019)
[![code quality](https://img.shields.io/static/v1.svg?label=code%20quality&message=clean%20code%2C%20solid%2C%20unusedcode&color=#97cabf)](https://github.com/jmsilvadev/Moloni2019)
[![code style](https://img.shields.io/static/v1.svg?label=code%20style&message=PSR2%20PSR4&color=#97cabf)](https://github.com/jmsilvadev/Moloni2019)

### Getting Started

#### Prerequisites

You need to install:
- PHP ^7.1;
- Composer;

### To use in Others Projects

Copy the directory `Moloni` in the project and include this lines below in the `composer.json` file:

```
"autoload": {
        "psr-4": {
            "Moloni\\": [
                "Moloni/",
                "Moloni/Config/",
                "Moloni/Moloni/"
            ]
        }
    }
```

Run the comand below to composer recognize the wrapper:

```
$composer dump-autload
```

***

### To use Cloning Repo

##### Installing

Clone the repo.

```
$git clone https://github.com/jmsilvadev/Moloni2019.git
$composer install
```
***

### Usage

To use Moloni Wrapper just instantiate a Class want to use, the wrapper have all funcionalities of the API Moloni. To see all funcionalities and your mandatory and response fields, please visit the page of API in https://www.moloni.pt/dev/?action=getApiDoc.

For example, if you want to include a client do this in the php file in your site:

1 - Include the autoload in your php file to can access the wrapper.

```
include 'vendor/autoload.php';
```

2 - Create an array with data to send to api.

```
$arrBody = [
            "company_id" => 65482,
            "name" => "Test Example",
            "number" => "CLI-9998", //Change to other code
            "vat" => "111111111", //Change to other vat number
            "address" => "Example Address",
            "city" => "Test Example",
            "zip_code" => "1111-111",
            "payment_day" => 5,
            "discount" => 5,
            "credit_limit" => 5,
            "delivery_method_id" => $arrDelivery[0]["delivery_method_id"],
            "salesman_id" => $arrSalesman[0]["salesman_id"],
            "language_id" => $arrLang[0]["language_id"],
            "country_id" => $arrCountries[1]["country_id"],
            "maturity_date_id" => $arrMatDates[0]["maturity_date_id"],
            "payment_method_id" => $arrPay[1]["payment_method_id"]
        ];
```

3 - Instantiate the class that needs to use, in this case we use Customers class:

```
$customers = new Customers();
```

4 - Call the method:

```
$arrRes = $customers->insert($arrBody);
```

5 - See the results in the array of response:

```
echo "INSERT CLIENT:" . PHP_EOL;
print_r($arrRes);
```

***

### Tests

This project follow [TDD](https://en.wikipedia.org/wiki/Test-driven_development) and uses automated tests.

#### Running the tests

```
vendor/bin/phpunit
```

#### Code Coverage

After running tests a directory with the coverage reporta are generated in `./Tests/Reports/coverage/ `
Verify if directory exists, in negative case, create dir:

```
$mkdir ./Tests/Reports/coverage
```

#### Checking CodeStyle

```
vendor/bin/phpcs --error-severity=1 --warning-severity=8 --extensions=php --standard=PSR2  Moloni/
```

#### Fix Errors in CodeStyle

```
vendor/bin/phpcbf --error-severity=1 --warning-severity=8 --extensions=php --standard=PSR2  Moloni/
```

#### Checking CodeQuality

```
vendor/bin/phpmd ./Moloni text cleancode,unusedcode
```
