<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsUsersTest extends TestCase
{
    public function testGetAll()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Users();
        $resp = $moloni->getAll($arrBody);
        if (!empty($resp["error"])) {
            $this->assertIsString($resp["error_description"]);
        } else {
            $this->assertArrayHasKey("user_id", $resp[0]);
        }
    }
}