<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;
use Moloni\GenericalTest;

class UnitsSuppliersTest extends TestCase
{
    private $generical;

    public function testCount()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Suppliers();
        $resp = $moloni->count($arrBody);
        $this->assertArrayHasKey("count", $resp);
    }

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482,
            "qty" => 1,
            "offset" => 0
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getAll($arrBody);

        if (count($resp) > 0) {
            $this->assertArrayHasKey("supplier_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testGetOne()
    {
        $arrBody = [
            "company_id" => 65482,
            "supplier_id" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("supplier_id", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1"
        ];

        $moloni = new Suppliers();
        $resp = $moloni->countBySearch($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1",
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getBySearch($arrBody);

        if (!empty($resp[0])) {
            $this->assertArrayHasKey("supplier_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByVat()
    {
        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451"
        ];

        $moloni = new Suppliers();
        $resp = $moloni->countByVat($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetByVat()
    {
        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451"
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getByVat($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("supplier_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByNumber()
    {
        $arrBody = [
            "company_id" => 65482,
            "number" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->countByNumber($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByNumber()
    {
        $arrBody = [
            "company_id" => 65482,
            "number" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getByNumber($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("supplier_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automted"
        ];

        $moloni = new Suppliers();
        $resp = $moloni->countByName($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automted"
        ];

        $moloni = new Suppliers();
        $resp = $moloni->getByName($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("supplier_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testInsert()
    {

        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451",
            "number" => "2",
            "name" => "Test Automted",
            "language_id" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1",
            "maturity_date_id" => "19",
            "copies" => [
                "document_type_id" => "1",
                "copies" => "1"
            ],
            "payment_method_id" => "1",
            "salesman_id" => 11497,
            "payment_day" => 14,
            "discount" => 5,
            "credit_limit" => 100,
            "qty_copies_document" => 1,
            "delivery_method_id" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "supplier_id" => 1,
            "vat" => "228187451",
            "number" => "111111111",
            "name" => "Test Automted",
            "language_id" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1",
            "maturity_date_id" => "19",
            "copies" => [
                "document_type_id" => "1",
                "copies" => "1"
            ],
            "payment_method_id" => "1",
            "salesman_id" => 11497,
            "payment_day" => 14,
            "discount" => 5,
            "credit_limit" => 100,
            "qty_copies_document" => 1,
            "delivery_method_id" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "supplier_id" => 1
        ];

        $moloni = new Suppliers();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testSuppliers()
    {
        $this->generical = new GenericalTest();
        $arrCount = ["company_id" => 65482];
        $arrInsert = [  "company_id" => 65482, "date" => "2019-07-12", "document_set_id" => 113518, "supplier_id" => 2,
                        "products" => [
                            "product_id" => 1, "name" => "Automated Test", "qty" => 1, "price" => 10,
                            "taxes" => ["tax_id" => 1],
                            "child_products" => [
                                "child_products" => [
                                    "product_id" => 1, "name" => "EX", "qty" => 1, "price" => 10,
                                    "taxes" => [
                                        "tax_id" => 1
                                        ]
                                ]
                            ]
                        ], 
                        "associated_documents" => [
                            "associated_id" => 1, "value" => 7
                        ]
        ];
 
        $arrUpdate = ["company_id" => 65482, "document_id" => 1, "date" => "2019-07-12", "document_set_id" => 113518, "supplier_id" => 2, "products" => [
            "product_id" => 1, "name" => "Automated Test", "qty" => 1, "price" => 10 ]];
        $arrDelete = ["company_id" => 65482, "document_id" => 1];

        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierCreditNotes", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierDebitNotes", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierPurchaseOrder", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierInvoices", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierInvoices", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierInvoices", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierInvoices", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierInvoices", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierInvoices", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierReceipts", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReceipts", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReceipts", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReceipts", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierReceipts", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierReceipts", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierReturnNotes", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierSimplifiedInvoices", "delete", $arrDelete, "valid");

        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "insert", $arrInsert, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "update", $arrUpdate, "supplier_id");
        $this->generical->genericalTest("\Moloni\SupplierWarrantyRequests", "delete", $arrDelete, "valid");
    }

}