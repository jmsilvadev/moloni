<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsProductCategoriesTest extends TestCase
{

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482,
            "parent_id" => 1
        ];

        $moloni = new ProductCategories();
        $resp = $moloni->getAll($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("category_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testInsert()
    {

        $arrBody = [
            "company_id" => 65482,
            "parent_id" => "1",
            "name" => "Test Automated"   
        ];

        $moloni = new ProductCategories();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "category_id" => 1102666,
            "parent_id" => "1",
            "name" => "Test Automated"   
        ];

        $moloni = new ProductCategories();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "category_id" => 1
        ];

        $moloni = new ProductCategories();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertNull($resp);
        }
    }
}