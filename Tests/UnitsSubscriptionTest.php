<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsSubscriptionTest extends TestCase
{
    public function testGetOne()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Subscription();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp["error"])) {
            $this->assertIsString($resp["error_description"]);
        } else {
            $this->assertArrayHasKey("subscription_id", $resp);
        }
    }
}