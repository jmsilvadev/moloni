<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsConfigTest extends TestCase
{
    public function testSetAuth()
    {
        $arr = [
            "DEVELOPER_ID"  => "",
            "CLIENT_SECRET" => "",
            "USERNAME" => "",
            "PASSWORD" => ""
        ];
        Config::setAuth($arr);
        $this->assertEquals(Config::$DEVELOPER_ID, $arr["DEVELOPER_ID"]);
    }
}