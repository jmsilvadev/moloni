<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsProductsTest extends TestCase
{

    public function testCount()
    {
        $arrBody = [
            "company_id" => 65482,
            "category_id" => 50
        ];
        $moloni = new Products();
        $resp = $moloni->count($arrBody);
        $this->assertArrayHasKey("count", $resp);
    }

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482,
            "category_id" => 1
        ];

        $moloni = new Products();
        $resp = $moloni->getAll($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("product_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testGetOne()
    {
        $arrBody = [
            "company_id" => 65482,
            "product_id" => 1
        ];

        $moloni = new Products();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("product_id", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1"
        ];

        $moloni = new Products();
        $resp = $moloni->countBySearch($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1",
        ];

        $moloni = new Products();
        $resp = $moloni->getBySearch($arrBody);

        if (!empty($resp[0])) {
            $this->assertArrayHasKey("product_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automated"
        ];

        $moloni = new Products();
        $resp = $moloni->countByName($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automted"
        ];

        $moloni = new Products();
        $resp = $moloni->getByName($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("product_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByReference()
    {
        $arrBody = [
            "company_id" => 65482,
            "reference" => "Test Automated"
        ];

        $moloni = new Products();
        $resp = $moloni->countByReference($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByReference()
    {
        $arrBody = [
            "company_id" => 65482,
            "reference" => "Test Automted"
        ];

        $moloni = new Products();
        $resp = $moloni->getByReference($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("product_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByEAN()
    {
        $arrBody = [
            "company_id" => 65482,
            "ean" => "Test Automated"
        ];

        $moloni = new Products();
        $resp = $moloni->countByEAN($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByEAN()
    {
        $arrBody = [
            "company_id" => 65482,
            "ean" => "Test Automted"
        ];

        $moloni = new Products();
        $resp = $moloni->getByEAN($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("product_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountModifiedSince()
    {
        $arrBody = [
            "company_id" => 65482,
            "lastmodified" => "Test Automted"
        ];

        $moloni = new Products();
        $resp = $moloni->countModifiedSince($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("count", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetModifiedSince()
    {
        $arrBody = [
            "company_id" => 65482,
            "lastmodified" => "2019-07-12"
        ];

        $moloni = new Products();
        $resp = $moloni->getModifiedSince($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("product_id", $resp[0])) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("product_id", $resp[0]);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }  

    public function testInsert()
    {

        $arrBody = [
            "company_id" => 65482,
            "category_id" => "228187451",
            "type" => "2",
            "name" => "Test Automted",
            "reference" => "1",
            "price" => "Address",
            "unit_id" => "City",
            "has_stock" => "1111-111",
            "stock" => "1"
        ];

        $moloni = new Products();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "product_id" => 1,
            "category_id" => "228187451",
            "type" => "2",
            "name" => "Test Automted",
            "reference" => "1",
            "price" => "Address",
            "unit_id" => "City",
            "has_stock" => "1111-111",
            "stock" => "1"
        ];

        $moloni = new Products();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "product_id" => 1
        ];

        $moloni = new Products();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertNull($resp);
        }
    }
}