<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsCustomersTest extends TestCase
{

    public function testCount()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Customers();
        $resp = $moloni->count($arrBody);
        $this->assertArrayHasKey("count", $resp);
    }

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482,
            "qty" => 50,
            "offset" => 0
        ];

        $moloni = new Customers();
        $resp = $moloni->getAll($arrBody);

        if (count($resp) > 0) {
            $this->assertArrayHasKey("customer_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testGetOne()
    {
        $arrBody = [
            "company_id" => 65482,
            "customer_id" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("customer_id", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1"
        ];

        $moloni = new Customers();
        $resp = $moloni->countBySearch($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetBySearch()
    {
        $arrBody = [
            "company_id" => 65482,
            "search" => "1",
        ];

        $moloni = new Customers();
        $resp = $moloni->getBySearch($arrBody);

        if (!empty($resp[0])) {
            $this->assertArrayHasKey("customer_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByVat()
    {
        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451"
        ];

        $moloni = new Customers();
        $resp = $moloni->countByVat($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetByVat()
    {
        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451"
        ];

        $moloni = new Customers();
        $resp = $moloni->getByVat($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("customer_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByNumber()
    {
        $arrBody = [
            "company_id" => 65482,
            "number" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->countByNumber($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByNumber()
    {
        $arrBody = [
            "company_id" => 65482,
            "number" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->getByNumber($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("customer_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testCountByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automted"
        ];

        $moloni = new Customers();
        $resp = $moloni->countByName($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("count", $resp);
        } else {
            $this->assertNull($resp);
        }
    }

    public function testGetByName()
    {
        $arrBody = [
            "company_id" => 65482,
            "name" => "Test Automted"
        ];

        $moloni = new Customers();
        $resp = $moloni->getByName($arrBody);
        if (!empty($resp[0])) {
            $this->assertArrayHasKey("customer_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetLastNumber()
    {
        $arrBody = [
            "company_id" => 65482
        ];

        $moloni = new Customers();
        $resp = $moloni->getLastNumber($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("number", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetNextNumber()
    {
        $arrBody = [
            "company_id" => 65482
        ];

        $moloni = new Customers();
        $resp = $moloni->getNextNumber($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("number", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testInsert()
    {
        
        $arrBody = [
            "company_id" => 65482,
            "vat" => "228187451",
            "number" => "2",
            "name" => "Test Automted",
            "language_id" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "2",
            "maturity_date_id" => "19",
            "copies" => [
                "document_type_id" => "1",
                "copies" => "1"
            ],
            "payment_method_id" => "1",
            "salesman_id" => 11497,
            "payment_day" => 14,
            "discount" => 5,
            "credit_limit" => 100,
            "delivery_method_id" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "customer_id" => 1,
            "vat" => "228187451",
            "number" => "111111111",
            "name" => "Test Automted",
            "language_id" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1",
            "maturity_date_id" => "19",
            "copies" => [
                "document_type_id" => "1",
                "copies" => "1"
            ],
            "payment_method_id" => "1",
            "salesman_id" => 11497,
            "payment_day" => 14,
            "discount" => 5,
            "credit_limit" => 100,
            "delivery_method_id" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "customer_id" => 1
        ];

        $moloni = new Customers();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertNull($resp);
        }
    }
}