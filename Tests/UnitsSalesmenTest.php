<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsSalesmenTest extends TestCase
{

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482
        ];

        $moloni = new Salesmen();
        $resp = $moloni->getAll($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("salesman_id", $resp[0]);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testGetOne()
    {
        $arrBody = [
            "company_id" => 65482,
            "salesman_id" => 1
        ];

        $moloni = new Salesmen();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("salesman_id", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testInsert()
    {
        
        $arrBody = [
            "company_id" => 65482,
            "name" => "TestSalesmen",
            "vat" => "228187451",
            "number" => "2",
            "base_commission" => 10.00,
            "language_id" => "1",
            "qty_copies_document" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1"
        ];

        $moloni = new Salesmen();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "salesman_id" => "11497",
            "name" => "TestSalesmen",
            "vat" => "228187451",
            "number" => "1",
            "base_commission" => 10.00,
            "language_id" => "1",
            "qty_copies_document" => "1",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1"
        ];

        $moloni = new Salesmen();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "salesman_id" => 1
        ];

        $moloni = new Salesmen();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
                echo "To works correctly you need to configure Moloni environment";
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertNull($resp);
        }
    }
}