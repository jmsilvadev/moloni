<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsCompanyTest extends TestCase
{

    public function testFreeSlug()
    {
        $moloni = new Company();
        $resp = $moloni->freeSlug("teste de slug");
        if (!empty($resp["error"])) {
            $this->assertEquals("valid input", $resp["error"]);
        } else {
            $this->assertArrayHasKey("valid", $resp);
        }
    }

    public function testGetAll()
    {
        $moloni = new Company();
        $resp = $moloni->getAll();
        if (!empty($resp["error"])) {
            $this->assertIsString($resp["error_description"]);
        } else {
            $this->assertArrayHasKey("company_id", $resp[0]);
        }
    }

    public function testGetOne()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Company();
        $resp = $moloni->getOne($arrBody);
        if (!empty($resp["error"])) {
            $this->assertIsString($resp["error_description"]);
        } else {
            $this->assertArrayHasKey("company_id", $resp);
        }
    }
}