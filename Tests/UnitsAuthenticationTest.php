<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;
use Moloni\Config;

class UnitsAuthenticationTest extends TestCase
{

    private $filename;

    public function testGetAccessTokenWithNoFileStorage()
    {
        $this-> filename = __DIR__ . "/../Moloni/Config" . Config::$FILE_TOKENS;
        @unlink($this->filename);

        $moloni = new Authentication();
        $resp = $moloni->getAccessToken();
        if (!empty($tokens["error"])) {
            $this->assertTrue($resp["error"]);
        } else {
            $this->assertArrayHasKey("access_token", $resp);
            $this->assertArrayHasKey("refresh_token", $resp);
        }
    }

    public function testSetFileTokens()
    {
        $arrToken = [
            "access_token" => "7cd597942686da6ecbdba41cf57b0abed69d3e55",
            "refresh_token" => "96f4a8c9e20ec1d343ebff7c0e9138ca2f6aa28b"
        ];

        $moloni = new Authentication();
        $resp = $moloni->setFileTokens($arrToken);
        if (!empty($tokens["error"])) {
            $this->assertTrue($resp["error"]);
        } else {
            $this->assertFalse($resp["error"]);
        }

        @unlink($this->filename);
    }

    public function testSetAccessToken()
    {
        $arrToken = [
            "access_token" => "7cd597942686da6ecbdba41cf57b0abed69d3e55",
            "refresh_token" => "96f4a8c9e20ec1d343ebff7c0e9138ca2f6aa28b"
        ];
        $moloni = new Authentication();
        $resp = $moloni->setAccessToken($arrToken);
        if (!empty($tokens["error"])) {
            $this->assertTrue($resp["error"]);
        } else {
            $this->assertArrayHasKey("access_token_expiration", $resp);
            $this->assertArrayHasKey("refresh_token_expiration", $resp);
        }

    }

    public function testGetAccessTokenWithFileStorage()
    {
        $moloni = new Authentication();
        $resp = $moloni->getStorageToken();
        if (!empty($tokens["error"])) {
            $this->assertTrue($resp["error"]);
        } else {
            $this->assertArrayHasKey("access_token", $resp);
            $this->assertArrayHasKey("refresh_token", $resp);
        }

    }

    public function testGetAccessToken()
    {
        $moloni = new Authentication();
        $resp = $moloni->getAccessToken();
        if (!empty($tokens["error"])) {
            $this->assertTrue($resp["error"]);
        } else {
            $this->assertArrayHasKey("access_token", $resp);
            $this->assertArrayHasKey("refresh_token", $resp);
        }
    }

}