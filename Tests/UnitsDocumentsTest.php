<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;
use Moloni\GenericalTest;

class UnitsDocumentsTest extends TestCase
{
    private $generical;

    public function testDocuments()
    {
        $this->generical = new GenericalTest();
        $arrCount = ["company_id" => 65482];
        $arrInsert = [  "company_id" => 65482, "net_value" => 10, "expiration_date" => "2019-07-20", "value" => 10, "date" => "2019-07-12", "document_set_id" => 113518, "customer_id" => 2,
                        "products" => [
                            "product_id" => 1, "name" => "Automated Test", "qty" => 1, "price" => 10,
                            "taxes" => ["tax_id" => 1],
                            "child_products" => [
                                "child_products" => [
                                    "product_id" => 1, "name" => "EX", "qty" => 1, "price" => 10,
                                    "taxes" => [
                                        "tax_id" => 1
                                        ]
                                ]
                            ]
                        ], 
                        "associated_documents" => [
                            "associated_id" => 1, "value" => 7
                        ]
        ];
 
        $arrUpdate = ["company_id" => 65482, "value" => 10, "net_value" => 10, "expiration_date" => "2019-07-20", "document_id" => 1, "date" => "2019-07-12", "document_set_id" => 113518, "customer_id" => 2, "products" => [
            "product_id" => 1, "name" => "Automated Test", "qty" => 1, "price" => 10 ]];
        $arrDocument = ["company_id" => 65482, "document_id" => 1];
        $arrModified = ["company_id" => 65482, "lastmodified" => "2019-07-12"];
        $arrTransport = ["company_id" => 65482, "document_id" => 1, "transport_code" => "CODE"];

        $this->generical->genericalTest("\Moloni\Documents", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Documents", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Documents", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Documents", "getAllDocumentTypes", ["language_id" => 1], "url");
        $this->generical->genericalTest("\Moloni\Documents", "getPDFLink", $arrDocument, "url");
        $this->generical->genericalTest("\Moloni\Documents", "countModifiedSince", $arrModified, "count");
        $this->generical->genericalTest("\Moloni\Documents", "getModifiedSince", $arrModified, "document_id");

        $this->generical->genericalTest("\Moloni\Invoices", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Invoices", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Invoices", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Invoices", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\Invoices", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\Invoices", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\Invoices", "generateMBReference", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\SimplifiedInvoices", "generateMBReference", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\Receipts", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Receipts", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Receipts", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Receipts", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\Receipts", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\Receipts", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\CreditNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CreditNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CreditNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CreditNotes", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\CreditNotes", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\CreditNotes", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\DebitNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DebitNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DebitNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DebitNotes", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\DebitNotes", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\DebitNotes", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\DeliveryNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\DeliveryNotes", "setTransportCode", $arrTransport, "valid");

        $this->generical->genericalTest("\Moloni\BillsOfLading", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\BillsOfLading", "setTransportCode", $arrTransport, "valid");

        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\OwnAssetsMovementGuides", "setTransportCode", $arrTransport, "valid");

        $this->generical->genericalTest("\Moloni\Waybills", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Waybills", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Waybills", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Waybills", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\Waybills", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\Waybills", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\Waybills", "setTransportCode", $arrTransport, "valid");

        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\CustomerReturnNotes", "setTransportCode", $arrTransport, "valid");

        $this->generical->genericalTest("\Moloni\Estimates", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Estimates", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Estimates", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\Estimates", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\Estimates", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\Estimates", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\InternalDocuments", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\InternalDocuments", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\InternalDocuments", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\InternalDocuments", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\InternalDocuments", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\InternalDocuments", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\InvoiceReceipts", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\invoiceReceipts", "generateMBReference", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\PaymentReturns", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PaymentReturns", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PaymentReturns", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PaymentReturns", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\PaymentReturns", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\PaymentReturns", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\PurchaseOrder", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PurchaseOrder", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PurchaseOrder", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\PurchaseOrder", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\PurchaseOrder", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\PurchaseOrder", "delete", $arrDocument, "valid");

        $this->generical->genericalTest("\Moloni\GlobalGuides", "count", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "getAll", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "getOne", $arrCount, "count");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "insert", $arrInsert, "document_id");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "update", $arrUpdate, "document_id");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "delete", $arrDocument, "valid");
        $this->generical->genericalTest("\Moloni\GlobalGuides", "setTransportCode", $arrTransport, "valid");

    }

}