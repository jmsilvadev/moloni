<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsProductStocksTest extends TestCase
{

    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482
        ];

        $moloni = new ProductStocks();
        $resp = $moloni->getAll($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("product_stock_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testInsert()
    {

        $arrBody = [
            "company_id" => 65482,
            "product_id" => "1",
            "movement_date" => "2019-07-12 11:30:07",
            "qty" => "Test Automated"
        ];

        $moloni = new ProductStocks();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "product_stock_id" => "1",
            "product_id" => "1",
            "movement_date" => "2019-07-12 11:30:07"
        ];

        $moloni = new ProductStocks();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "product_stock_id" => 1
        ];

        $moloni = new ProductStocks();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            $this->assertArrayHasKey("valid", $resp);
        } else {
            $this->assertNull($resp);
        }
    }
}