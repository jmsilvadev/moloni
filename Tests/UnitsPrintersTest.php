<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsPrintersTest extends TestCase
{
    public function testRequestPrintJob()
    {
        $arrBody = [
            "company_id" => "65482",
            "printer_id" => 0,
            "daemon_id" => 0,
            "open_drawer" => 0,
            "document_id" => 0
        ];

        $moloni = new Printers();
        $resp = $moloni->requestPrintJob($arrBody);
        if (!empty($resp["error"])) {
            $this->assertIsString($resp["error_description"]);
        } else {
            $this->assertArrayHasKey("valid", $resp);
        }
    }

    public function testGetCompanyPrinters()
    {
        $arrBody = [ "company_id" => 65482 ];
        $moloni = new Printers();
        $resp = $moloni->getCompanyPrinters($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("daemon_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }
}