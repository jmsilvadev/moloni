<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;
use Moloni\GenericalTest;

class UnitsSettingsTest extends TestCase
{
    private $generical;

    public function testSettings()
    {
        $this->generical = new GenericalTest();
        $arrCount = ["company_id" => 65482];
       
        $arrFull = [  "company_id" => 65482, "ordination_missing" => 1, "maturity_date_id" => 1, "bank_account_id" => 1,
                        "name" => "Test Aut", "days" => 1, "associated_discount" => 10, "code" => 1,
                        "eac_id" => 1, "is_numerary" => 1, "payment_method_id" => 1, "delivery_method_id" => 1,
                        "description" => "TestAut", "vehicle_id" => 1, "deduction_id" => 1, "tax_id" => 1,
                        "short_name" => "TestAut", "unit_id" => 1, "business_name" => "TestAut", "template_id" => 1,
                        "document_set_id" => 1, "title" => "Test Aut", "warehouse_id" => 1, "property_id" => 1,
                        "ordination" => 1, "lastmodified" => "2019-07-12", "email" => "development@rdfonseca.com",
                        "value" => 7.21, "number_plate" => "JHJ-TEST", "address" => "Test Automated", "type" => 1,
                        "city" => "Test", "saft_type" => 1
                    ];

        $this->generical->genericalTest("\Moloni\BankAccounts", "getAll", $arrCount, "bank_account_id");
        $this->generical->genericalTest("\Moloni\BankAccounts", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\BankAccounts", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\BankAccounts", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\EconomicActivityClassificationCodes", "getAll", $arrCount, "eac_id");
        $this->generical->genericalTest("\Moloni\EconomicActivityClassificationCodes", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\EconomicActivityClassificationCodes", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\EconomicActivityClassificationCodes", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\PaymentMethods", "getAll", $arrCount, "payment_method_id");
        $this->generical->genericalTest("\Moloni\PaymentMethods", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\PaymentMethods", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\PaymentMethods", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\MaturityDates", "getAll", $arrCount, "maturity_date_id");
        $this->generical->genericalTest("\Moloni\MaturityDates", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MaturityDates", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MaturityDates", "delete", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MaturityDates", "countModifiedSince", $arrFull, "count");
        $this->generical->genericalTest("\Moloni\MaturityDates", "getModifiedSince", $arrFull, "count");

        $this->generical->genericalTest("\Moloni\DeliveryMethods", "getAll", $arrCount, "delivery_method_id");
        $this->generical->genericalTest("\Moloni\DeliveryMethods", "insert", $arrFull, "delivery_method_id");
        $this->generical->genericalTest("\Moloni\DeliveryMethods", "update", $arrFull, "delivery_method_id");
        $this->generical->genericalTest("\Moloni\DeliveryMethods", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\Vehicles", "getAll", $arrCount, "vehicle_id");
        $this->generical->genericalTest("\Moloni\Vehicles", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Vehicles", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Vehicles", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\Deductions", "getAll", $arrCount, "deduction_id");
        $this->generical->genericalTest("\Moloni\Deductions", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Deductions", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Deductions", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\Taxes", "getAll", $arrCount, "tax_id");
        $this->generical->genericalTest("\Moloni\Taxes", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Taxes", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Taxes", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\MeasurementUnits", "getAll", $arrCount, "unit_id");
        $this->generical->genericalTest("\Moloni\MeasurementUnits", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MeasurementUnits", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MeasurementUnits", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\IdentificationTemplates", "getAll", $arrCount, "template_id");
        $this->generical->genericalTest("\Moloni\IdentificationTemplates", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\IdentificationTemplates", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\IdentificationTemplates", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\DocumentSets", "getAll", $arrCount, "document_set_id");
        $this->generical->genericalTest("\Moloni\DocumentSets", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\DocumentSets", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\DocumentSets", "delete", $arrFull, "valid");

        $this->generical->genericalTest("\Moloni\Warehouses", "getAll", $arrCount, "warehouse_id");
        $this->generical->genericalTest("\Moloni\Warehouses", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Warehouses", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Warehouses", "delete", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\Warehouses", "countModifiedSince", $arrFull, "count");

        $this->generical->genericalTest("\Moloni\ProductProperties", "getAll", $arrCount, "property_id");
        $this->generical->genericalTest("\Moloni\ProductProperties", "insert", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\ProductProperties", "update", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\ProductProperties", "delete", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\ProductProperties", "countModifiedSince", $arrFull, "count");
        $this->generical->genericalTest("\Moloni\ProductProperties", "getModifiedSince", $arrFull, "property_id");
    }

}