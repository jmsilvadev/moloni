<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsCustomerAlternateAddressesTest extends TestCase
{


    public function testGetAll()
    {
        $arrBody = [
            "company_id" => 65482,
            "customer_id" => 1
        ];

        $moloni = new CustomerAlternateAddresses();
        $resp = $moloni->getAll($arrBody);
        
        if (!empty($resp)) {
            $this->assertArrayHasKey("customer_id", $resp);
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testInsert()
    {
        
        $arrBody = [
            "company_id" => 65482,
            "customer_id" => "1",
            "designation" => "111111111",
            "code" => "Test Automted",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1"
        ];

        $moloni = new CustomerAlternateAddresses();
        $resp = $moloni->insert($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testUpdate()
    {
        $arrBody = [
            "company_id" => 65482,
            "address_id" => "1",
            "customer_id" => "1",
            "designation" => "111111111",
            "code" => "Test Automted",
            "address" => "Address",
            "city" => "City",
            "zip_code" => "1111-111",
            "country_id" => "1"
        ];

        $moloni = new CustomerAlternateAddresses();
        $resp = $moloni->update($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testDelete()
    {
        $arrBody = [
            "company_id" => 65482,
            "address_id" => "1",
            "customer_id" => 1
        ];

        $moloni = new CustomerAlternateAddresses();
        $resp = $moloni->delete($arrBody);
        if (isset($resp)) {
            if (!array_key_exists("valid", $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey("valid", $resp);
            }
        } else {
            $this->assertNull($resp);
        }
    }
}