<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;
use Moloni\GenericalTest;

class UnitsMyProfileTest extends TestCase
{
    private $generical;

    public function testMyProfile()
    {
        $this->generical = new GenericalTest();
       
        $arrFull = [    "name" => "Test Automated", "email" => "development@rdfonseca.com", "password" => "TestAuto",
                        "language_id" => 1, "company" => 65482, "vat" => 1, "slug" => "TestAut",
                        "country_id" => 1, "cellphone" => "111111111"
                    ];

        $this->generical->genericalTest("\Moloni\MyProfile", "signUp", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MyProfile", "recoverPassword", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MyProfile", "updateMe", $arrFull, "valid");
        $this->generical->genericalTest("\Moloni\MyProfile", "getMe", $arrFull, "user_id");

    }

}