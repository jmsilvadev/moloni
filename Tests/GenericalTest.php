<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class GenericalTest extends TestCase
{
    public function genericalTest($model, $method, $arrBody, $key)
    {
        
        $obj = new $model();
        $resp = $obj->$method($arrBody);
        if (!empty($resp)) {
            if (!array_key_exists($key, $resp)) {
                $this->assertNotEmpty($resp);
            } else {
                $this->assertArrayHasKey($key, $resp);
            }
        } else {
            $this->assertEmpty($resp);
        }
    }

    public function testFalsePositive()
    {
        $this->assertTrue(TRUE, 'To not inform false possitives.');
    }
}