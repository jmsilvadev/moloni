<?php
namespace Moloni;

use PHPUnit\Framework\TestCase;

class UnitsGlobalDataTest extends TestCase
{

    public function testCountriesGetAll()
    {
        $moloni = new Countries();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("country_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testCurrenciesGetAll()
    {
        $moloni = new Currencies();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("currency_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testFiscalZonesGetAll()
    {
        $arrBody = [
            "country_id" => "1"
        ];
        $moloni = new FiscalZones();
        $resp = $moloni->getAll($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("country_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testLanguagesGetAll()
    {
        $moloni = new Languages();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("language_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testDocumentModelsGetAll()
    {
        $moloni = new DocumentModels();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("document_model_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }
    
    public function testTaxExemptionsGetAll()
    {
        $moloni = new TaxExemptions();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("code", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testCurrencyExchangeGetAll()
    {
        $moloni = new CurrencyExchange();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("currency_exchange_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testCurrencyExchangeGetModifiedSince()
    {
        $arrBody = [
            "lastmodified" => "2019-07-12"
        ];

        $moloni = new CurrencyExchange();
        $resp = $moloni->getModifiedSince($arrBody);
        if (count($resp) > 0) {
            $this->assertArrayHasKey("currency_exchange_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testMBGatewaysGetAll()
    {
        $moloni = new MBGateways();
        $resp = $moloni->getAll();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("gateway_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }

    public function testMBGatewaysGetModifiedSince()
    {
        $moloni = new MBGateways();
        $resp = $moloni->getModifiedSince();
        if (count($resp) > 0) {
            $this->assertArrayHasKey("gateway_id", $resp[0]);
        } else {
            $this->assertIsArray($resp);
        }
    }
}