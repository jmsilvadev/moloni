<?php
namespace Moloni;

use Moloni\Config;

class MaturityDates
{

    public function getAll($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "maturityDates/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "name", "days", "associated_discount"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "maturityDates/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "maturity_date_id", "name", "days", "associated_discount"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "maturityDates/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        $arrMandatory = ["company_id", "maturity_date_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "maturityDates/delete/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "maturityDates/countModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "maturityDates/getModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
