<?php
namespace Moloni;

use Moloni\Config;

class MyProfile
{

    public function signUp($arrBody)
    {
        $arrMandatory = ["name", "email", "password", "language_id", "company", "vat", "slug", "country_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "users/signUp/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function recoverPassword($arrBody)
    {
        $arrMandatory = ["email"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "users/recoverPassword/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function updateMe($arrBody)
    {
        $arrMandatory = ["name", "email", "cellphone", "language_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "users/updateMe/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getMe($arrBody)
    {
        $url =  Config::URL_API . "users/getMe/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
