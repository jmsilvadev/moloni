<?php
namespace Moloni;

class Curl extends Authentication
{
    public function execCurl($url, $postfields = null)
    {
        $con = curl_init();

        curl_setopt($con, CURLOPT_URL, $url);
        curl_setopt($con, CURLOPT_POST, true);
        if (!empty($postfields)) {
            curl_setopt($con, CURLOPT_POSTFIELDS, http_build_query($postfields));
        }
        curl_setopt($con, CURLOPT_HEADER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($con, CURLOPT_CONNECTTIMEOUT, Config::CURLOPT_CONNECTTIMEOUT);
        curl_setopt($con, CURLOPT_TIMEOUT, Config::CURLOPT_TIMEOUT);

        $res_curl = curl_exec($con);
        curl_close($con);

        $result = json_decode($res_curl, true);

        if (isset($result['error'])) {
            return $result;
        }
        
        return $result;
    }

    public function factoryCurl($urlPartial, $arrBody = null)
    {
        $tokens = $this->getAccessToken();

        if (!empty($tokens["error"])) {
            return $tokens;
        }

        $url =  $urlPartial . "?access_token=".$tokens["access_token"];
        $curl = new Curl();
        $arrCurl = $curl->execCurl($url, $arrBody);

        if (isset($arrCurl["error_description"])) {
            if ($arrCurl["error_description"] == "Invalid access token.") {
                $filename = __DIR__ . Config::$FILE_TOKENS;
                @unlink($filename);
                $this->factoryCurl($urlPartial, $arrBody);
            }
        }

        return $arrCurl;
    }
}
