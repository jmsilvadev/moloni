<?php
namespace Moloni;

use Moloni\Config;

class Company
{
    public function freeSlug($slug)
    {
        $arrBody = [
            "slug" => $slug
        ];
        $url =  Config::URL_API . "companies/freeSlug/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getAll()
    {
        $url =  Config::URL_API . "companies/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }

    public function getOne($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
        
        $url =  Config::URL_API . "companies/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
