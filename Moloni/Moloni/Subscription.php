<?php
namespace Moloni;

use Moloni\Config;

class Subscription
{
    public function getOne($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "subscription/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
