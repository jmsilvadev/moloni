<?php
namespace Moloni;

use Moloni\Config;

class Languages
{
    public function getAll()
    {
        $url =  Config::URL_API . "languages/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
