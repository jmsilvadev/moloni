<?php
namespace Moloni;

use Moloni\Config;

class Salesmen
{
    public function getAll($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        $url =  Config::URL_API . "salesmen/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getOne($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("salesman_id", $arrBody)) {
            return [
                "error" => "salesman_id missing",
                "error_description"  => "the field salesman_id is mandatory"
            ];
        }

        $url =  Config::URL_API . "salesmen/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "vat", "number", "name", "base_commission", "language_id", "qty_copies_document", "address", "city", "country_id"];

        if (isset($arrBody["country_id"]) && $arrBody["country_id"] == "1") {
            $arrMandatory[] = "zip_code";
        }

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "salesmen/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "salesman_id", "vat", "number", "name", "base_commission", "language_id", "qty_copies_document", "address", "city", "country_id"];

        if (isset($arrBody["country_id"]) && $arrBody["country_id"] == "1") {
            $arrMandatory[] = "zip_code";
        }

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "salesmen/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("salesman_id", $arrBody)) {
            return [
                "error" => "salesman_id missing",
                "error_description"  => "the field salesman_id is mandatory"
            ];
        }

        $url =  Config::URL_API . "salesmen/delete/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }
}
