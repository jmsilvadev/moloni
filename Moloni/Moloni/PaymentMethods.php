<?php
namespace Moloni;

use Moloni\Config;

class PaymentMethods
{

    public function getAll($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "paymentMethods/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "name", "is_numerary"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "paymentMethods/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "payment_method_id", "name", "is_numerary"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "paymentMethods/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        $arrMandatory = ["company_id", "payment_method_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "paymentMethods/delete/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
