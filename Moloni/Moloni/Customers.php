<?php
namespace Moloni;

use Moloni\Config;

class Customers
{
    public function count($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
        $url =  Config::URL_API . "customers/count/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getAll($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getOne($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("customer_id", $arrBody)) {
            return [
                "error" => "customer_id missing",
                "error_description"  => "the field customer_id is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countBySearch($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("search", $arrBody)) {
            return [
                "error" => "search missing",
                "error_description"  => "the field search is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/countBySearch/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getBySearch($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("search", $arrBody)) {
            return [
                "error" => "search missing",
                "error_description"  => "the field search is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getBySearch/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByVat($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("vat", $arrBody)) {
            return [
                "error" => "vat missing",
                "error_description"  => "the field vat is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/countByVat/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByVat($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("vat", $arrBody)) {
            return [
                "error" => "vat missing",
                "error_description"  => "the field vat is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getByVat/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByNumber($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("number", $arrBody)) {
            return [
                "error" => "number missing",
                "error_description"  => "the field number is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/countByNumber/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByNumber($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("number", $arrBody)) {
            return [
                "error" => "number missing",
                "error_description"  => "the field number is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getByNumber/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByName($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("name", $arrBody)) {
            return [
                "error" => "name missing",
                "error_description"  => "the field name is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/countByName/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByName($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("name", $arrBody)) {
            return [
                "error" => "name missing",
                "error_description"  => "the field name is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getByName/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getLastNumber($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getLastNumber/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getNextNumber($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/getNextNumber/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "vat", "number", "name", "language_id", "address", "city", "country_id", "maturity_date_id", "payment_method_id"];
        
        if (isset($arrBody["country_id"]) && $arrBody["country_id"] == "1") {
            $arrMandatory[] = "zip_code";
        }

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["copies"])) {
            if (!array_key_exists("copies", $arrBody["copies"])) {
                return [
                    "error" => "copies missing",
                    "error_description"  => "the field copies is mandatory"
                ];
            }

            if (!array_key_exists("document_type_id", $arrBody["copies"])) {
                return [
                    "error" => "document_type_id missing",
                    "error_description"  => "the field document_type_id is mandatory"
                ];
            }
        }

        if (strlen($arrBody["number"]) > 20) {
            return [
                "error" => "lenght exceed",
                "error_description"  => "the field number must be only 20 characters"
            ];
        }
            
        $url =  Config::URL_API . "customers/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "customer_id", "vat", "number", "name", "language_id", "address", "city", "country_id", "maturity_date_id", "copies", "payment_method_id"];

        if (isset($arrBody["country_id"]) && $arrBody["country_id"] == "1") {
            $arrMandatory[] = "zip_code";
        }

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
// @codeCoverageIgnoreEnd
            }
        }

        if (!array_key_exists("document_type_id", $arrBody["copies"])) {
            return [
                "error" => "document_type_id missing",
                "error_description"  => "the field document_type_id is mandatory"
            ];
        }

        if (!array_key_exists("copies", $arrBody["copies"])) {
            return [
                "error" => "copies missing",
                "error_description"  => "the field copies is mandatory"
            ];
        }

        if (strlen($arrBody["number"]) > 20) {
            return [
                "error" => "lenght exceed",
                "error_description"  => "the field number must be only 20 characters"
            ];
        }
            
        $url =  Config::URL_API . "customers/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("customer_id", $arrBody)) {
            return [
                "error" => "customer_id missing",
                "error_description"  => "the field customer_id is mandatory"
            ];
        }
            
        $url =  Config::URL_API . "customers/delete/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
