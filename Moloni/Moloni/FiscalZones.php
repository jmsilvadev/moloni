<?php
namespace Moloni;

use Moloni\Config;

class FiscalZones
{
    public function getAll($arrBody)
    {
        $arrMandatory = ["country_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "fiscalZones/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
