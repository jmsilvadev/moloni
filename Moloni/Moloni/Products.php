<?php
namespace Moloni;

use Moloni\Config;

class Products
{
    public function count($arrBody)
    {
        $arrMandatory = ["company_id", "category_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
        $url =  Config::URL_API . "products/count/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getAll($arrBody)
    {
        $arrMandatory = ["company_id", "category_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getOne($arrBody)
    {
        $arrMandatory = ["company_id", "product_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countBySearch($arrBody)
    {
        $arrMandatory = ["company_id", "search"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "products/countBySearch/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getBySearch($arrBody)
    {
        $arrMandatory = ["company_id", "search"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getBySearch/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByName($arrBody)
    {
        $arrMandatory = ["company_id", "name"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/countByName/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByName($arrBody)
    {
        $arrMandatory = ["company_id", "name"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getByName/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByReference($arrBody)
    {
        $arrMandatory = ["company_id", "reference"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/countByReference/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByReference($arrBody)
    {
        $arrMandatory = ["company_id", "reference"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getByReference/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countByEAN($arrBody)
    {
        $arrMandatory = ["company_id", "ean"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/countByEAN/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getByEAN($arrBody)
    {
        $arrMandatory = ["company_id", "ean"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getByEAN/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/countModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "products/getModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "category_id", "type", "name", "reference", "price", "unit_id", "has_stock", "stock"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["taxes"])) {
            $arrMandatory = ["tax_id", "value", "order", "cumulative"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["taxes"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["suppliers"])) {
            $arrMandatory = ["supplier_id", "cost_price"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["suppliers"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["properties"])) {
            $arrMandatory = ["property_id", "value"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["properties"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["warehouses"])) {
            $arrMandatory = ["warehouse_id", "stock"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["warehouses"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }
            
        $url =  Config::URL_API . "products/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "product_id", "category_id", "type", "name", "reference", "price", "unit_id", "has_stock", "stock"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["taxes"])) {
            $arrMandatory = ["tax_id", "value", "order", "cumulative"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["taxes"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["suppliers"])) {
            $arrMandatory = ["supplier_id", "cost_price"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["suppliers"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["properties"])) {
            $arrMandatory = ["property_id", "value"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["properties"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }
            
        $url =  Config::URL_API . "products/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        $arrMandatory = ["company_id", "product_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "products/delete/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
