<?php
namespace Moloni;

use Moloni\Config;

class SupplierPurchaseOrder
{

    public function count($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "supplierPurchaseOrder/count/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getAll($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "supplierPurchaseOrder/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getOne($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "supplierPurchaseOrder/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }


    public function insert($arrBody)
    {
        $arrMandatory = ["company_id", "date", "document_set_id", "supplier_id", "products"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["associated_documents"])) {
            $arrMandatory = ["associated_id", "value"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["associated_documents"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        $arrMandatory = ["product_id", "name", "qty", "price"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody["products"])) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["products"]["taxes"])) {
            $arrMandatory = ["tax_id"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["products"]["taxes"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }
        
        if (!empty($arrBody["products"]["child_products"])) {
            $arrMandatory = ["product_id", "name", "qty", "price"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["products"]["child_products"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        if (!empty($arrBody["products"]["child_products"]["taxes"])) {
            $arrMandatory = ["tax_id"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["products"]["child_products"]["taxes"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        $url =  Config::URL_API . "supplierPurchaseOrder/insert/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function update($arrBody)
    {
        $arrMandatory = ["company_id", "document_id", "date","document_set_id", "supplier_id", "products"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        if (!empty($arrBody["associated_documents"])) {
            $arrMandatory = ["associated_id", "value"];

            foreach ($arrMandatory as $arrField) {
                if (!array_key_exists($arrField, $arrBody["associated_documents"])) {
                    return [
                        "error" => $arrField . " missing",
                        "error_description"  => "the field ". $arrField . " is mandatory"
                    ];
                }
            }
        }

        $arrMandatory = ["product_id", "name", "qty", "price"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody["products"])) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "supplierPurchaseOrder/update/";
        $curl = new Curl();
        $resp = $curl->factoryCurl($url, $arrBody);

        if (isset($resp[0])) {
            return [
                "error" => "invalid fields",
                "error_description" => "The fields listed have problems or invalid data see errors list in https://www.moloni.pt/dev/?action=getApiTxtDetail&id=7",
                "fields" => $resp
            ];
        }
        return $resp;
    }

    public function delete($arrBody)
    {
        $arrMandatory = ["company_id", "document_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }

        $url =  Config::URL_API . "supplierPurchaseOrder/delete/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
