<?php
namespace Moloni;

use Moloni\Config;

class TaxExemptions
{
    public function getAll()
    {
        $url =  Config::URL_API . "taxExemptions/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
