<?php
namespace Moloni;

use Moloni\Config;

class Authentication
{
    /**
     * Checks if exists tokens in file tokens.expire, if dont exists then make a call to moloni
     * If exists but was expired then make a new call to moloni
     *
     * @return array
     */
    public function getAccessToken()
    {
        
        $isValidTokens = $this->getStorageToken();
        if (!empty($isValidTokens["error"]) || empty($isValidTokens["refresh_token"])) {
            $url =  Config::URL_API . "grant/?grant_type=password&client_id=" . Config::$DEVELOPER_ID . "&client_secret=" . Config::$CLIENT_SECRET . "&username=". Config::$USERNAME . "&password=" . urlencode(Config::$PASSWORD);
            $curl = new Curl();
            $arrToken = $curl->execCurl($url);
            if (!empty($arrToken["error"])) {
                return $arrToken;
            }

            $arrToken = [
                "access_token" => $arrToken["access_token"],
                "refresh_token" => $arrToken["refresh_token"]
            ];

            $this->setAccessToken($arrToken);
            return $arrToken;
        }

        if (empty($isValidTokens["access_token"])) {
            $url = Config::URL_API . "grant/?grant_type=refresh_token&client_id=" . Config::$DEVELOPER_ID . "&client_secret=" . Config::$CLIENT_SECRET . "&refresh_token=" . $isValidTokens["refresh_token"];
            $curl = new Curl();
            $arrToken = $curl->execCurl($url);
            if (!empty($tokens["error"])) {
                return $arrToken;
            }
            $arrToken = [
                "access_token" => $arrToken["access_token"],
                "refresh_token" => $arrToken["refresh_token"]
            ];

            $this->setAccessToken($arrToken);
            return $arrToken;
        }

        return $isValidTokens;
    }

    /**
     * Saves tokens in token.expires file
     *
     * @param array $arrToken
     * @return array
     */
    public function setFileTokens($arrToken)
    {
        $filename = __DIR__ . Config::$FILE_TOKENS;
        if (!$handle = fopen($filename, 'w')) {
            return [
                "error" => true,
                "data"  => "Cannot open file ($filename)"
            ];
        }

        $content = "";
        foreach ($arrToken as $key => $value) {
            $content .= $key . " = " . $value . "\n";
        }

        if (fwrite($handle, $content) === false) {
            return [
                "error" => true,
                "data"  => "Cannot write to file ($filename)"
            ];
        }

        fclose($handle);

        return [
            "error" => false,
            "data"  => "Success, wrote ($content) to file ($filename)"
        ];
    }

    /**
     * Saves tokens in a file to reuse avoiding unecessary calls to api
     *
     * @param array $arrToken
     * @return array
     */
    public function setAccessToken($arrToken)
    {
        $refresh_token_expiration = date_create('NOW', new \DateTimeZone(Config::TIMEZONE));
        $refresh_token_expiration->modify('+14 days');

        $access_token_expiration = date_create('NOW', new \DateTimeZone(Config::TIMEZONE));
        $access_token_expiration->modify('+59 minutes');

        $arrToken["access_token_expiration"] = date_format($access_token_expiration, 'Y-m-d H:i:s');
        $arrToken["refresh_token_expiration"] = date_format($refresh_token_expiration, 'Y-m-d');

        $arrResult = $this->setFileTokens($arrToken);

        if (!empty($arrResult["error"])) {
            return $arrResult;
        }

        return $arrToken;
    }

    /**
     * Gets tokens of a tokens.expires file and check if they are valid
     * If not, returns null in the tokens keys
     *
     * @param array $arrToken
     * @return array
     */
    public function getStorageToken()
    {
        $filename = __DIR__ . Config::$FILE_TOKENS;
        $handle = @fopen($filename, "r");
        if (!$handle) {
            return [
                "error" => true,
                "message"  => "Cannot open file ($filename)"
            ];
        }

        $token = [];
        while (($buffer = fgets($handle, 4096)) !== false) {
            $KeyValue = explode(" = ", $buffer);
            $key = $KeyValue[0];
            $value = rtrim($KeyValue[1]);
            $token[$key] = $value;
        }

        fclose($handle);

        $access_token_expiration = date_create($token["access_token_expiration"]);
        $access_now = date_create('NOW', new \DateTimeZone(Config::TIMEZONE));
        $access_interval = $access_now->diff($access_token_expiration);

        $refresh_token_expiration = date_create($token["refresh_token_expiration"]);
        $refresh_now = date_create('NOW', new \DateTimeZone(Config::TIMEZONE));
        $refresh_interval = $refresh_now->diff($refresh_token_expiration);
        
        $arrToken["access_token"] = strval($access_interval->format("%I")) > 1 ? $token["access_token"] : false;
        $arrToken["refresh_token"] = strval($refresh_interval->format("%d")) > 1 ? $token["refresh_token"] : false;

        return $arrToken;
    }
}
