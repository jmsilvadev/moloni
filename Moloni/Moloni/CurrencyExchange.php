<?php
namespace Moloni;

use Moloni\Config;

class CurrencyExchange
{
    public function getAll()
    {
        $url =  Config::URL_API . "currencyExchange/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }

    public function getModifiedSince($arrBody)
    {
        $arrMandatory = ["lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "currencyExchange/getModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
