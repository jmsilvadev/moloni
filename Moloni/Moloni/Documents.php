<?php
namespace Moloni;

use Moloni\Config;

class Documents
{

    public function getAllDocumentTypes($arrBody)
    {
        $arrMandatory = ["language_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/getAllDocumentTypes/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function count($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/count/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getAll($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getOne($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/getOne/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getPDFLink($arrBody)
    {
        $arrMandatory = ["company_id", "document_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/getPDFLink/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function countModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/countModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function getModifiedSince($arrBody)
    {
        $arrMandatory = ["company_id", "lastmodified"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
            
        $url =  Config::URL_API . "documents/getModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
