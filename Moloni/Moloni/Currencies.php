<?php
namespace Moloni;

use Moloni\Config;

class Currencies
{
    public function getAll()
    {
        $url =  Config::URL_API . "currencies/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
