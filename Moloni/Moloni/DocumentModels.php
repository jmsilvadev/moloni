<?php
namespace Moloni;

use Moloni\Config;

class DocumentModels
{
    public function getAll()
    {
        $url =  Config::URL_API . "documentModels/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
