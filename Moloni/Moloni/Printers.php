<?php
namespace Moloni;

use Moloni\Config;

class Printers
{
    public function getCompanyPrinters($arrBody)
    {
        $arrMandatory = ["company_id"];

        foreach ($arrMandatory as $arrField) {
            if (!array_key_exists($arrField, $arrBody)) {
                // @codeCoverageIgnoreStart
                return [
                    "error" => $arrField . " missing",
                    "error_description"  => "the field ". $arrField . " is mandatory"
                ];
                // @codeCoverageIgnoreEnd
            }
        }
        $url =  Config::URL_API . "printers/getCompanyPrinters/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }

    public function requestPrintJob($arrBody)
    {
        if (!array_key_exists("company_id", $arrBody)) {
            return [
                "error" => "company_id missing",
                "error_description"  => "the field company_id is mandatory"
            ];
        }

        if (!array_key_exists("printer_id", $arrBody)) {
            return [
                "error" => "printer_id missing",
                "error_description"  => "the field printer_id is mandatory"
            ];
        }

        if (!array_key_exists("daemon_id", $arrBody)) {
            return [
                "error" => "daemon_id missing",
                "error_description"  => "the field daemon_id is mandatory"
            ];
        }

        if (!array_key_exists("document_id", $arrBody)) {
            return [
                "error" => "document_id missing",
                "error_description"  => "the field document_id is mandatory"
            ];
        }

        $url =  Config::URL_API . "printers/requestPrintJob/";
        $curl = new Curl();
        return $curl->factoryCurl($url, $arrBody);
    }
}
