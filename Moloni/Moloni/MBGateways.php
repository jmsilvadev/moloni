<?php
namespace Moloni;

use Moloni\Config;

class MBGateways
{
    public function getAll()
    {
        $url =  Config::URL_API . "mBGateways/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }

    public function getModifiedSince()
    {
            
        $url =  Config::URL_API . "mBGateways/getModifiedSince/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
