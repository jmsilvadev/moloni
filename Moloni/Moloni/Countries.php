<?php
namespace Moloni;

use Moloni\Config;

class Countries
{
    public function getAll()
    {
        $url =  Config::URL_API . "countries/getAll/";
        $curl = new Curl();
        return $curl->factoryCurl($url);
    }
}
