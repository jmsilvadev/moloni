<?php
namespace Moloni;

final class Config
{
    // @codeCoverageIgnoreStart
    public static $DEVELOPER_ID = "";
    public static $CLIENT_SECRET = "";
    public static $USERNAME = "";
    public static $PASSWORD = "";
    public static $FILE_TOKENS = "";
    public const TIMEZONE = "Europe/Lisbon";
    public const URL_API = "https://api.moloni.pt/v1/";
    public const CURLOPT_CONNECTTIMEOUT = 5; // in seconds
    public const CURLOPT_TIMEOUT = 60; // in seconds
    // @codeCoverageIgnoreEnd

    static public function setAuth($arr) {
        SELF::$DEVELOPER_ID = $arr["DEVELOPER_ID"];
        SELF::$CLIENT_SECRET = $arr["CLIENT_SECRET"];
        SELF::$USERNAME = $arr["USERNAME"];
        SELF::$PASSWORD = $arr["PASSWORD"];
        SELF::$FILE_TOKENS = "/../Config/tokens_expire." . $arr["DEVELOPER_ID"];
        $filename = __DIR__ . Config::$FILE_TOKENS;
        @unlink($filename);
       
    }
}
